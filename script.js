const messageContainer = document.getElementById('message-container');
const messages = ['Quizas no pudimos ver el eclipse... Pero por eso cree este programa', 'Para que lo puedas ver siempre que quieras...', 'Te quiero mucho mi amor! <3'];
let messageIndex = 0;

function showMessage() {
    if (messageIndex < messages.length) {
        const messageElement = document.createElement('p');
        messageElement.textContent = messages[messageIndex];
        
        if (messageIndex == 0) {
            messageElement.style.left = '0%';
        } else if (messageIndex === messages.length - 1) {
            messageElement.style.left = '50%'; 
        }
        
        messageContainer.appendChild(messageElement);
        messageIndex++;
    }
}

setInterval(showMessage, 5000);
